public class ForLoops {
    public static void main(String[] args) {
        // looks like javascript!!!
//        for (int counter = 1; counter <= 5; counter++) {
//            System.out.println(counter);
//        }

//        for (double rate = 2.0; rate <= 5.0; rate += 0.25) {
//            double interestAmount = calculateInterest(100.00, rate);
//            if (interestAmount > 4.0) {
//                System.out.println("The interest amount is " + interestAmount + ". YOU CAN'T AFFORD THAT!");
//                break;
//            }
//            System.out.println("The interest for $100.00 at " + rate + "% "+ " is " + interestAmount);
//        }

//        int count = 0;
//        for (int i = 0; i <= 50; i++) {
//            boolean prime = isPrime(i);
//            if (prime) {
//                count++;
//            }
//            System.out.println("" + i + " is prime? " + prime);
//        }
//        System.out.println("From 0 to 50, there are " + count + " prime numbers");

        int sum = 0;
        int count = 0;
        for (int i = 1; i <= 1000; i++) {
            if (divisibleByThreeAndFive(i)) {
                System.out.println(i);
                sum += i;
                count++;
            }
            if (count == 5) {
                break;
            }
        }
        System.out.println(sum);
    }

    public static double calculateInterest(double amount, double interestRate) {
        return amount * (interestRate/100);
    }

    public static boolean isPrime(int wholeNumber) {
        if (wholeNumber <= 2) {
            return wholeNumber == 2;
        }
        for (int divisor = 2; divisor < wholeNumber; divisor++) {
            if (wholeNumber % divisor == 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean divisibleByThreeAndFive(int wholeNumber) {
        return wholeNumber % 3 == 0 && wholeNumber % 5 == 0;
    }
}
