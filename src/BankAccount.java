public class BankAccount {
    private String number;
    private double balance;
    private String customerName;
    private String customerEmail;
    private String customerPhone;

    public BankAccount() {
        // you can call the other constructor below with "this(parameters)" in this empty constructor to specify default parameters
        // if this empty constructor is call, must be the very first line
        System.out.println("Empty constructor called");
    }

    // constructor overloading like method overloading
    // the rule of thumb is to use constructors and not setters, because it guarantees that all fields are satisfied
    public BankAccount(String number, double balance, String customerName, String customerEmail, String customerPhone) {
//        System.out.println("Account constructor with parameters called");
        this.number = number;
        this.balance = balance;
        this.customerName = customerName;
        this.customerEmail = customerEmail;
        this.customerPhone = customerPhone;
    }

    public void depositFunds(double amount) {
        balance += amount;
        System.out.println("You deposited $" + amount + ". Your new balance is $" + this.balance);
    }

    public void withdrawFunds(double amount) {
        if (balance - amount < 0) {
            System.out.println("Insufficient funds. You only have $" + this.balance + " in your account.");
        } else {
            balance -= amount;
            System.out.println("You withdrew $" + amount + ". Your new balance is $" + this.balance);
        }
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }
}
