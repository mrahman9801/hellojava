public class MethodChallenges {
    public static void main(String[] args) { // you call functions in main basically
//        printConversion(414.23);
//        printMegaBytesAndKiloBytes(2500);
//        boolean wakeUp = shouldWakeUp(true, -1);
//        System.out.println(wakeUp);
//        boolean leapYear = isLeapYear(2017);
//        System.out.println(leapYear);
//        boolean equalByThree = areEqualByThreeDecimalPlaces(3.175, 3.176);
//        System.out.println(equalByThree);
//        boolean thereIsTeen = hasTeen(9, 99, 20);
//        System.out.println(thereIsTeen);
    }

    public static long toMilesPerHour(double kilometersPerHour) {
        if (kilometersPerHour >= 0) {
            long rounded = Math.round(kilometersPerHour/1.609d);
            return rounded;
        }
        return -1;
    }

    public static void printConversion(double kilometersPerHour) {
        long milesPerHour = toMilesPerHour(kilometersPerHour);
        if (kilometersPerHour >= 0) {
            System.out.println("" + kilometersPerHour + " km/h " + "= " + milesPerHour + " mi/h");
        } else {
            System.out.println("Invalid Value");
        }
    }

    public static void printMegaBytesAndKiloBytes(int kiloBytes) {
        if (kiloBytes >= 0) {
            int KB = kiloBytes % 1024;
            int MB = Math.floorDiv(kiloBytes, 1024);
            System.out.println("" + kiloBytes + " KB " + "= " + MB + " MB and " + KB + " KB");
        } else {
            System.out.println("Invalid Value");
        }
    }

    public static boolean shouldWakeUp(boolean barking, int hourOfDay) {
        if (hourOfDay < 0 || hourOfDay > 23) {
            return false;
        }
        if (barking && ((hourOfDay > 22) || (hourOfDay < 8))) {
            return true;
        }
        return false;
    }

    public static boolean isLeapYear(int year) {
        if (year < 1 || year > 9999) {
            return false;
        }
        if ((year % 4 == 0) && (year % 100 == 0) && (year % 400 == 0)) {
            return true;
        } else if ((year % 4 == 0) && (year % 100 != 0)) {
            return true;
        }
        return false;
    }

    public static boolean areEqualByThreeDecimalPlaces(double numOne, double numTwo) {
        int first = (int) (numOne * 1000);
        int second = (int) (numTwo * 1000);
        return first == second;
    }

    public static boolean hasTeen(int paraOne, int paraTwo, int paraThree) {
        return ((paraOne > 12 && paraOne < 20) || (paraTwo > 12 && paraTwo < 20) || (paraThree > 12 && paraThree < 20));
    }

}
