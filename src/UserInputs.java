import java.util.Scanner;

public class UserInputs {
    public static void main(String[] args) {

        String yearOfBirth = "1999";
        int yearOfBirthNum = Integer.parseInt(yearOfBirth); // like conversions in python/javascript
        // Double.parseDouble is also a thing, among others
//        System.out.println(yearOfBirthNum);
        int currentYear = 2023;

//        try {
//            System.out.println(getInputFromConsole(currentYear));
//        } catch (NullPointerException e) {
//            System.out.println(getInputFromScanner(currentYear));
//        }

        System.out.println(gimmeFiveNumbers());

    }

    public static String getInputFromConsole(int currentYear) {
        // you have to run this file on terminal for this command to work (ides have it turned off)
        String name = System.console().readLine("Hey what's your name? ");
        System.out.println("Sup " + name);
        String yearOfBirth = System.console().readLine("What year you born? ");
        int age = currentYear - Integer.parseInt(yearOfBirth);
        return "So you're " + age;
    }

    public static String getInputFromScanner(int currentYear) {

        // create scanner object that has methods, like special objects imported in other languages
        Scanner scanner = new Scanner(System.in);

//        String name = System.console().readLine("Hey what's your name? ");
        System.out.println("Hey, what's your name? ");
        String name = scanner.nextLine();
        System.out.println("Hey " + name);
//        String yearOfBirth = System.console().readLine("What year you born? ");
        System.out.println("What year were you born in? ");
        boolean validDOB = false;
        int age = 0;
        do {
            System.out.println("Enter a year of birth >= " + (currentYear-125) + " and <= " + currentYear);
            try {
                age = checkData(currentYear, scanner.nextLine());
                validDOB = age >= 0;
            } catch (NumberFormatException e) {
                System.out.println("Nah don't put letters in a number bozo.");
            }
        } while (!validDOB);

        return "So you're " + age;
    }

    public static int checkData(int currentYear, String dateOfBirth) {
        int dob = Integer.parseInt(dateOfBirth);
        int minimumYear = currentYear - 125;
        if ((dob < minimumYear) || (dob > currentYear)) {
            return -1;
        }
        return (currentYear - dob);
    }

    public static String gimmeFiveNumbers() {
        Scanner scanner = new Scanner(System.in);
        int counter = 1;
        int sum = 0;
        while (counter <= 5) {
            System.out.println("Enter whole number # " + counter + ":");
            String nextNumber = scanner.nextLine();
            try {
                int number = Integer.parseInt(nextNumber);
                counter++;
                sum += number;
            } catch (NumberFormatException e) {
                System.out.println("Nah that's not valid...");
            }
        }
        return "The total of these numbers is " + sum;
    }
}
