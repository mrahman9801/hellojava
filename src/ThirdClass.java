public class ThirdClass {
    public static void main(String[] args) {
        String firstScore = calculateScore(200, true, 5);
        String secondScore = calculateScore(1, false, 3);
        System.out.println(firstScore);
        System.out.println(secondScore);
    }

    public static String calculateScore(int score, boolean isOver, int levelsCompleted) {
        int bonus = 25;
        int finalScore = score + (levelsCompleted * bonus);
        if (isOver) {
            return "Your final score is " + finalScore;
        } else {
            return "Your current score is " + finalScore;
        }
    }
}
