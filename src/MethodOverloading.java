public class MethodOverloading {
    public static void main(String[] args) {
//        double centimeters = convertCentimeters(5, 10);
//        System.out.println(centimeters);
        printYearsAndDays(1051200);
    }

    public static double convertToCentimeters(int inches) {
        return inches * 2.54;
    }

    // Java doesn't allow optional parameters like python, so you have to do this (method overloading).
    // Methods can have the same name, if the ordering of parameters or the parameters themselves are different.
    // The compiler knows which method is being called based on what arguments are being passed.

    public static double convertCentimeters(int feet, int inches) {
        return convertToCentimeters((feet * 12) + inches);
    }

    public static double area(double radius) {
        if (radius < 0) {
            return -1;
        }
        return (radius * radius) * Math.PI;
    }

    public static double area(double x, double y) {
        if (x < 0 || y < 0) {
            return -1;
        }
        return x * y;
    }

    public static void printYearsAndDays(long minutes) {
        if (minutes < 0) {
            System.out.println("Invalid Value");
        } else {
            int minutesInDay = 60 * 24;
            int days = (int) Math.floorDiv(minutes, minutesInDay);
            int years = Math.floorDiv(days, 365);
            int remainingDays = days % 365;
            System.out.println("" + minutes + " min = " + years + " y and " + remainingDays + " d");
        }
    }

    public static void printEqual(int paraOne, int paraTwo, int paraThree) {
        if (paraOne < 0 || paraTwo < 0 || paraThree < 0) {
            System.out.println("Invalid Value");
        } else if (paraOne == paraTwo && paraTwo == paraThree) {
            System.out.println("All numbers are equal");
        } else if (paraOne != paraTwo && paraTwo != paraThree && paraOne != paraThree) {
            System.out.println("All numbers are different");
        } else {
            System.out.println("Neither all are equal or different");
        }
    }

}
