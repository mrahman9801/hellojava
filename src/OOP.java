public class OOP {
    public static void main(String[] args) {

//        Car car = new Car();
//        car.setMake("Porsche");
//        car.setColor("Blue");
//        car.setModel("911");
//        car.setConvertible(true);
//        car.setDoors(2);
//        car.setYear(2023);
//        car.describeCar();

//        BankAccount moeAccount = new BankAccount();
//        moeAccount.depositFunds(250);
//        moeAccount.depositFunds(50.25);
//        moeAccount.withdrawFunds(120.99);
//        System.out.println(moeAccount.getBalance());

//        Calculator simpleCalculator = new Calculator();
//        simpleCalculator.setFirstNumber(4.5);
//        simpleCalculator.setSecondNumber(6);
//        simpleCalculator.doAddition();
//        simpleCalculator.doDivision();
//        simpleCalculator.doMultiplication();
//        simpleCalculator.doSubtraction();

//        Person randomDude = new Person();
//        randomDude.setLastName("Smith");
//        randomDude.setFirstName("");
//        randomDude.setAge(14);
//        randomDude.getFullName();
//        System.out.println("Teen? " + randomDude.isTeen());

        BankAccount stevesAccount = new BankAccount(
                "21421",
                0,
                "Steve Smith",
                "steve@email.com",
                "123-456-7890"
        );

        stevesAccount.depositFunds(100);
    }
}
