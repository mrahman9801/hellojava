public class SwitchStatements {
    public static void main(String[] args) {
//        getQuarter("June");
//        System.out.println(getQuarterBetter("Mays"));
//        printDayOfWeek(99);
        printNumberInWord(0);
    }

    public static void whatIsValue(int value) {
        // can't use long, float, double, boolean, or their wrappers in switch
        // this is enhanced switch syntax, looks cleaner
        switch (value) {
            case 1 -> System.out.println("Value is 1");
            case 2 -> System.out.println("Value is 2");
            case 3, 4, 5 -> { // braces because multiple lines
                System.out.println("Value was a 3 or 4 or 5");
                System.out.println("It was " + value);
            }
            default -> System.out.println("Was not in range 1-5");
        }
    }

    public static void getQuarter(String month) {
        // too freaking long
        switch (month) {
            case "January":
            case "February":
            case "March":
                System.out.println("1st");
                break;
            case "April":
            case "May":
            case "June":
                System.out.println("2nd");
                break;
            case "July":
            case "August":
            case "September":
                System.out.println("3rd");
                break;
            case "October":
            case "November":
            case "December":
                System.out.println("4th");
                break;
            default:
                System.out.println("Enter a valid month");
                break;
        }
    }

    public static String getQuarterBetter(String month) {
        return switch (month) {
            case "January", "February", "March" -> "1st";
            case "April", "May", "June" -> "2nd";
            case "July", "August", "September" -> "3rd";
            case "October", "November", "December" -> "4th";
            default -> {
                String badResponse = month + " is not a valid month";
                yield badResponse; // if multiline code-block, use yield
            }
        };
    }

    public static void printDayOfWeek(int day) {
        String dayOfWeek = switch (day) {
            case 0 -> "Sunday";
            case 1 -> "Monday";
            case 2 -> "Tuesday";
            case 3 -> "Wednesday";
            case 4 -> "Thursday";
            case 5 -> "Friday";
            case 6 -> "Saturday";
            default -> "Invalid Day";
        };
        System.out.println("It is " + dayOfWeek);
    }

    public static void printNumberInWord(int number) {
        String day = switch (number) {
            case 0 -> "ZERO";
            case 1 -> "ONE";
            case 2 -> "TWO";
            case 3 -> "THREE";
            case 4 -> "FOUR";
            case 5 -> "FIVE";
            case 6 -> "SIX";
            case 7 -> "SEVEN";
            case 8 -> "EIGHT";
            case 9 -> "NINE";
            default -> "OTHER";
        };
        System.out.println(day);
    }
}
