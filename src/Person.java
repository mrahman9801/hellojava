public class Person {

    private String firstName;
    private String lastName;
    private int age;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age > 120 || age < 0) {
            this.age = 0;
        } else {
            this.age = age;
        }
    }

    public void getFullName() {
        if (this.firstName.isEmpty() && this.lastName.isEmpty()) {
            System.out.println("Person has no name?");
        } else if (this.lastName.isEmpty()) {
            System.out.println("Person's name is " + this.firstName + ". They have no last name.");
        } else if (this.firstName.isEmpty()) {
            System.out.println("Person's last name is " + this.lastName + ". They have no first name...for some reason.");
        }
    }

    public boolean isTeen() {
        return this.age >= 13 && this.age <= 19;
    }
}
