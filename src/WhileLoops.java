public class WhileLoops {
    public static void main(String[] args) {

//        int j = 1;
//        while (j <= 5) {
//            System.out.println(j);
//            j++;
//        }
//
//        boolean isReady = false;
//        do { // Will execute do block at least once. If while denys it, then it won't execute  again.
//            if (j > 5) {
//                break;
//            }
//            System.out.println(j);
//            j++;
//            isReady = j > 0;
//        } while (isReady);

//        int num = 0;
//        while (num < 50) {
//            num += 5;
//            if (num % 25 == 0) {
//                continue;
//            }
//            System.out.print(num + "_");
//        }

//        int number = 4;
//        int finishNumber = 20;
//        int evenCount = 0;
//        int oddCount = 0;
//        while (number <= finishNumber) {
//            number++;
//            if (!isEvenNumber(number)) {
//                oddCount++;
//                continue;
//            }
//            System.out.println("Even number " + number);
//            evenCount++;
//            if (evenCount >= 5) {
//                break;
//            }
//        }
//        System.out.println(evenCount);
//        System.out.println(oddCount);
        System.out.println("The sum of all the digits in 123456 are " + digitSum(123456));
    }

    public static boolean isEvenNumber(int number) {
        return number % 2 == 0;
    }

    public static int digitSum(int number) {
        if (number < 0) {
            return -1;
        }
        int sum = 0;
        while (number > 9) {
            sum += (number % 10);
            number = (number / 10);
        }
        sum += number;
        return sum;
    }

    public static boolean isPalindrome(int number) {
        // make it even
        if (number < 0) {
            number *= -1;
        }
        int reverse = 0;
        int stored = number;
        while (stored > 0) {
            // grab digit in ones place
            int digit = stored % 10;
            // make reverse move up one place
            reverse = (reverse*10) + digit;
            // make number move down one place
            stored /= 10;
        }
        return number == reverse;
    }

    public static int sumFirstAndLastDigit(int number) {
        if (number < 0) {
            return -1;
        }
        int lastDigit = number % 10;
        while (number > 9) {
            number /= 10;
        }
        return number + lastDigit;
    }

    public static int getEvenDigitSum(int number) {
        if (number < 0) {
            return -1;
        }
        int sum = 0;
        while (number > 0) {
            int currentDigit = number % 10;
            if (currentDigit % 2 == 0) {
                sum += currentDigit;
            }
            number /= 10;
        }
        return sum;
    }

    public static boolean hasSharedDigit(int x, int y) {
        if (x > 99 || x < 10 || y > 99 || y < 10) {
            return false;
        }
        return (x/10 == y/10 || x%10 == y%10 || x/10 == y%10 || y/10 == x%10);
    }

    public static boolean isValid(int num) {
        return num >= 10 && num <= 1000;
    }

    public static boolean hasSameLastDigit(int x, int y, int z) {
        if (isValid(x) && isValid(y) && isValid(z)) {
            int xLastDigit = x%10;
            int yLastDigit = y%10;
            int zLastDigit = z%10;
            return (xLastDigit == yLastDigit || xLastDigit == zLastDigit || yLastDigit == zLastDigit);
        }
        return false;
    }

    public static boolean canPack(int bigCount, int smallCount, int goal) {
        // big = 5, small = 1
        if (smallCount < 0 || bigCount < 0 || goal < 0) {
            return false;
        }
        // use up all big bags
        while (bigCount > 0 && goal >= 5) {
            goal-=5;
            bigCount--;
        }
        // check if small bags can fill remaining
        return smallCount >= goal;
    }

    public static int getLargestPrime(int num) {
        if (num < 2) {
            return -1;
        }
        for (int i = num-1; i >= 2; i--) {
            if (num % i == 0) {
                // reassign num to biggest factor
                num = i;
                // num will keep being divided until it finds a prime number
            }
        }
        return num;
    }
}
