public class Calculator {
    private double firstNumber;
    private double secondNumber;

    public double getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(double firstNumber) {
        this.firstNumber = firstNumber;
    }

    public double getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(double secondNumber) {
        this.secondNumber = secondNumber;
    }

    public void doAddition() {
        System.out.println(this.firstNumber + " + " + this.secondNumber + " = " + (this.firstNumber + this.secondNumber));
    }

    public void doSubtraction() {
        System.out.println(this.firstNumber + " - " + this.secondNumber + " = " + (this.firstNumber - this.secondNumber));
    }

    public void doMultiplication() {
        System.out.println(this.firstNumber + " * " + this.secondNumber + " = " + (this.firstNumber * this.secondNumber));
    }

    public void doDivision() {
        System.out.println(this.firstNumber + " / " + this.secondNumber + " = " + (this.firstNumber / this.secondNumber));
    }
}
